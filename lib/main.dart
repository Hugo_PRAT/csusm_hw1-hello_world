import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          width: 280,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Hello world', style: TextStyle(fontSize: 34, color: Colors.blue), textAlign: TextAlign.center,),
              SizedBox(height: 20),
              Text("HUGO PRAT", style: TextStyle(fontSize: 24, color: Colors.red), textAlign: TextAlign.center,),
              Text("Spring 2021", style: TextStyle(fontSize: 24, color: Colors.red), textAlign: TextAlign.center,),
              SizedBox(height: 20),
              Text("On rencontre sa destiné souvent par les chemins qu'on prend pour l'éviter\n (it's in french)",
              style: TextStyle(fontSize: 14, color: Colors.deepPurple), textAlign: TextAlign.center,),
          ],
        ),
      ),)
    );
  }
}
